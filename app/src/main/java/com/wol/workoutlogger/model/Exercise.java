package com.wol.workoutlogger.model;

public class Exercise {
    private int id;

    private String name;

    private int fkWorkout;

    public Exercise() {
    }

    public Exercise(int id, String name, int fkWorkout) {
        this.id = id;
        this.name = name;
        this.fkWorkout = fkWorkout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFkWorkout() {
        return fkWorkout;
    }

    public void setFkWorkout(int fkWorkout) {
        this.fkWorkout = fkWorkout;
    }
}
