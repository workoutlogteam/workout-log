package com.wol.workoutlogger.model;

public class Series {

    private int id;

    private int reps;

    private int weight;

    private int time;

    private int fkIdExercise;

    public Series() {
    }

    public Series(int id, int reps, int weight, int time, int fkIdExercise) {
        this.id = id;
        this.reps = reps;
        this.weight = weight;
        this.time = time;
        this.fkIdExercise = fkIdExercise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getFkIdExercise() {
        return fkIdExercise;
    }

    public void setFkIdExercise(int fkIdExercise) {
        this.fkIdExercise = fkIdExercise;
    }
}
