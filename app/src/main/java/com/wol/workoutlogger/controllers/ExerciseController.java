package com.wol.workoutlogger.controllers;

import com.wol.workoutlogger.model.Exercise;
import com.wol.workoutlogger.model.Workout;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.logging.Logger;

@RestController
public class ExerciseController {

    private static final Logger LOGGER = Logger.getLogger(ExerciseController.class.getName());

    @GetMapping(value = "/v1/workout/getExercise/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Exercise> getExercise(@PathVariable(value = "id") int id) {
        return new ResponseEntity<Exercise>(new Exercise(), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/workout/getExerciseByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Exercise> getExerciseByName(@PathVariable(value = "name") String name) {
        return new ResponseEntity<Exercise>(new Exercise(), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/workout/getExerciseByDate/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Exercise> getExerciseByDate(@PathVariable(value = "date") Date date) {
        return new ResponseEntity<Exercise>(new Exercise(), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/workout/addExercise", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Exercise> addExercise(@RequestBody Exercise exercise) {
        return new ResponseEntity<Exercise>(new Exercise(), HttpStatus.OK);
    }

    @PutMapping(value = "/v1/workout/editExercise/{id}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> editExercise(@PathVariable int id, @PathVariable String name) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/workout/deleteWorkout/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> deleteWorkout(@PathVariable int id) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }
}
