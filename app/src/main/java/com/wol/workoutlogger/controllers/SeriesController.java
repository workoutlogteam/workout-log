package com.wol.workoutlogger.controllers;

import com.wol.workoutlogger.model.Series;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
public class SeriesController {

    private static final Logger LOGGER = Logger.getLogger(SeriesController.class.getName());

    @GetMapping(value = "/v1/workout/getSeriesByExercise/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> getSeriesByExercise(@PathVariable(value = "id") int id) {
        return new ResponseEntity<Series>(new Series(), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/workout/getSeriesByExerciseName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> getSeriesByExerciseName(@PathVariable(value = "name") String name) {
        return new ResponseEntity<Series>(new Series(), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/workout/addSeries", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> addSeries(@RequestBody Series series) {
        return new ResponseEntity<Series>(new Series(), HttpStatus.OK);
    }

    @PutMapping(value = "/v1/workout/editSeriesReps/{id}/{reps}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> editSeriesReps(@PathVariable int id, @PathVariable int reps) {
        return new ResponseEntity<Series>( new Series() , HttpStatus.OK);
    }

    @PutMapping(value = "/v1/workout/editSeriesWeight/{id}/{weight}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> editSeriesWeight(@PathVariable int id, @PathVariable int weight) {
        return new ResponseEntity<Series>( new Series() , HttpStatus.OK);
    }

    @PutMapping(value = "/v1/workout/editSeriesTime/{id}/{time}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> editSeriesTime(@PathVariable int id, @PathVariable int time) {
        return new ResponseEntity<Series>( new Series() , HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/workout/deleteSeries/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Series> deleteSeries(@PathVariable int id) {
        return new ResponseEntity<Series>( new Series() , HttpStatus.OK);
    }

}
