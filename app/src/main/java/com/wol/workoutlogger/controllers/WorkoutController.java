package com.wol.workoutlogger.controllers;

import com.wol.workoutlogger.model.Workout;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class WorkoutController {

    private static final Logger LOGGER = Logger.getLogger(WorkoutController.class.getName());

    @GetMapping(value = "/v1/workout/getAllworkouts", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<Workout>> getAllworkouts() {
        return new ResponseEntity<List<Workout>>(Arrays.asList(new Workout()), HttpStatus.OK);
    }

    @GetMapping(value = "/v1/workout/getWorkoutById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> getWorkoutById(@PathVariable(value = "id") int id ) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @GetMapping(value = "/v1/workout/getWorkoutByDate/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> getWorkoutByDate(@PathVariable(value = "date") LocalDate date ) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @GetMapping(value = "/v1/workout/getLastWorkout", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> getLastWorkout() {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @PostMapping(value = "/v1/workout/addWorkout", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> addWorkout(@RequestBody Workout workout) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @PutMapping(value = "/v1/workout/editWorkoutName/{id}/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> editWorkoutName(@PathVariable int id, @PathVariable String name) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @PutMapping(value = "/v1/workout/editWorkoutDate/{id}/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> editWorkoutDate(@PathVariable int id, @PathVariable Date name) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }

    @DeleteMapping(value = "/v1/workout/deletExercise/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Workout> deletExercise(@PathVariable int id) {
        return new ResponseEntity<Workout>( new Workout() , HttpStatus.OK);
    }


}
